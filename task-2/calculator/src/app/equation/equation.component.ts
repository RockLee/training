import { Component, OnInit, AfterContentInit } from '@angular/core';
import * as d3 from "d3";

@Component({
  selector: 'app-equation',
  templateUrl: './equation.component.html',
  styleUrls: ['./equation.component.scss']
})
export class EquationComponent implements OnInit {

  squareParmeter = 0
  linearParmeter = 0
  constantParameter = 0

  constructor() { 
    this.squareParmeter = 1;
    this.linearParmeter = 1;
    this.constantParameter = 1;
  }

  ngOnInit() {
    this.reDrawCurve(this.squareParmeter, this.linearParmeter, this.constantParameter)
  }
  
  reDrawCurve(squareP, linearP, constantP) {
    //Use the margin convention practice 
    var margin = {top: 30, right: 30, bottom: 30, left: 30};
    var width = 400;
    var height = 400;

    var xDomainMin = -10;
    var xDomainMax = 10;
    var yDomainMin = -10;
    var yDomainMax = 10; 

    //定義用於座標軸的線性比例尺
  	var xScale = d3.scaleLinear()
                   .domain([xDomainMin, xDomainMax])
                   .range([margin.left, width-margin.right])
    var yScale = d3.scaleLinear()
                   .domain([d3.max([yDomainMax, d3.max(xScale.domain(), function(d){ return squareP*d*d+linearP*d+constantP })]), 
                            d3.min([yDomainMin, d3.min(xScale.domain(), function(d){ return squareP*d*d+linearP*d+constantP})])])
                   .range([margin.top, height-margin.bottom])

    //建立一個svg元素
    d3.select('svg').remove()

    var svg = d3.select('#CalRight')
                .append('svg')
                .attr("width", width)
                .attr("height", height)

    //定義座標軸
    var xAxis = d3.axisBottom()
                  .scale(xScale);
    var yAxis = d3.axisLeft()
                  .scale(yScale);
    //在svg中新增一個包含座標軸各元素的g元素
    var gXAxis = svg.append('g')
                    .attr("transform", 'translate(0, 200)')
                    .call(xAxis)
    var gYAxis = svg.append('g')
                    .attr('transform', 'translate(200, 0)')
                    .call(yAxis)

//An array of objects of length N. Each object has key -> value pair, the key being "y" and the value is a random number
var dataset = d3.range(-10, 11, 1).map(function(d) { return {"y": squareP*d*d+linearP*d+constantP } })
//d3's line generator
var line = d3.line()
  .x(function(d, i) { return xScale(i); }) // set the x values for the line generator
  .y(function(d) { return yScale(d.y); }) // set the y values for the line generator 
  .curve(d3.curveMonotoneX) // apply smoothing to the line

//Append the path, bind the data, and call the line generator 
svg.append("path")
.datum(dataset) //Binds data to the line 
.attr("class", "line") // Assign a class for styling 
.attr("fill", "none")
.attr("stroke", "steelblue")
.attr("stroke-width", 1.5)
.attr("stroke-linejoin", "round")
.attr("stroke-linecap", "round")
.attr("transform", 'translate(-170, 0)')
.attr("d", line); //Calls the line generator
  }
}
