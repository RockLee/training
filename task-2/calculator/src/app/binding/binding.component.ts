import { Component, VERSION } from '@angular/core';

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.scss']
})
export class BindingComponent {
  name = "Angular";
  version = VERSION.full;
  counter: number = 0;

  constructor() { 
    setInterval(() => {
      this.counter++;
    }, 1000);
  }

  onClickButton(): void {
    alert("Hello " + this.name);
  }

}
