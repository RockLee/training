<h1 style="text-align:center">Front end probation document for Angular 6 and TypeScript</h1>

<br/>  

<h3 style="text-align:center">Design Specification</h3>
<h3 style="text-align:center">Release</h3>

<br/>  
<br/>  

<h3 style="text-align:center">Version 1.0</h3>

<br/>  
<br/>  

<h3 style="text-align:center">Date: Feb 24, 2018</h3>
<h3 style="text-align:center">Author: Egg Hsu</h3>
<h3 style="text-align:center">Reviewer: N/A</h3>

<br/>  
<br/>

THIS DOCUMENT CONTAINS PROPRIETARY TECHNICAL INFORMATION THAT IS THE PROPERTY OF THE Henbosche AND SHOULD NOT BE DISCLOSED TO OTHERS IN WHOLE OR IN PART, REPRODUCED, COPIED, OR USED AS BASIS FOR DESIGN, MANUFACTURING OR SALE OF APPARATUS WITHOUT WRITTEN PERMISSION OF Henbosche


## Revision History

| Date | Release | Author | Description | Version |
| ------ | ------ | ------ | ------ | ------ |
| 2018/02/24 | V1.0   | Egg Hsu   | Initial Release | V1.0  |


## Related Documents

| No. | Date | Author | Document |
| ------ | ------ | ------ | ------ |
| 1| | | |
| 2| | | |
| 3| | | |

## Contents




## Description

You are training as a Front End Developer, here is your probation assignment and reference document.

## Target of Goal

* MVC concept
* Git operation, how to use git.
* HTML W3C concepts.
* Delegation / Notification.
* Design patterns.
* Async call versus sync call (in angular: promise).
* Unit test.
* Modularize your code (Packages) to Node package or bower package.
* Node.js, NPM, grunt, bower
* .....慢慢加

## Reference

* [MVC, or MVW concept](https://stackoverflow.com/questions/13329485/what-does-mvw-stand-for)
* [Git usage: 連猴子都能懂的Git入門指南](https://backlogtool.com/git-tutorial/tw/)
* [Node.js](https://nodejs.org/en/)
* [NPM](https://www.npmjs.com/)
* [Grunt](https://gruntjs.com/)
* [Bower](https://bower.io/)
* [Angular](https://angularjs.org/)
* [Angular Material](https://material.angularjs.org/latest/)
* [D3.js](https://d3js.org/)


## Rules

* Use standard components, i.e, no third party packages, unless the task assignment is  specifically told you so. If you would like to use other third party packages, please ask your mentor beforehand.
* Please follow MVC, MVW concept for implementation.
* __<font color="red">Please push your progress AT LEAST once per day</font>__, if any code has been modified.
* Each task should have its own folder, ex: `task-1`, `task-2`, this might vary depends on the circumstances.
* Each task should begin with an empty initial commit to record your start time, and a complete commit to record your finish time.
* Code must confront Coding style, please checkout the following link for reference: [TypeScript StyleGuide and Coding Conventions](typescript-coding-guideline.md).
* With each task completion, please review the issues and write a guideline in README.md, including what you or your team members should do during implementation.
* Please solve one issue by one commit at a time, do not solve multiple issues with one single commit. We can build a knowledge database if the issue were solved individually.
* When solving issues, please write the git commit message like “Fix issue #xx” to sync the commit with particular issue number.

## Editor

### Atom

* __Restart Atom when package installed.__
* Packages(Mandatory):
  * [Angularjs](https://atom.io/packages/angularjs)
  * [Atom-beautify](https://atom.io/packages/atom-beautify)
  * [Atom-easy-jsdoc](https://atom.io/packages/atom-easy-jsdoc)
  * [Json-colorer](https://atom.io/packages/json-colorer)
  * [Language-ngdoc](https://atom.io/packages/language-ngdoc)
  * [language-yang](https://atom.io/packages/language-yang)
  * [Markdown-toc](https://atom.io/packages/markdown-toc)
  * [markdown-scroll-sync](https://atom.io/packages/markdown-scroll-sync)
  * [Minimap-git-diff](https://atom.io/packages/minimap-git-diff)
  * [Split-diff](https://atom.io/packages/split-diff)
  * [Svg-preview](https://atom.io/packages/svg-preview)

* Packages(Optional):
  * [Linter](https://atom.io/packages/linter)
  * [Linter-jscs](https://atom.io/packages/linter-jscs)
  * [Linter-jshint](https://atom.io/packages/linter-jshint)

* Editor setting:

![Editor Setting 1](./images/editor_setting_1.png)

![Editor Setting 2](./images/editor_setting_2.png)

* Code snippets:

  Please add the following to your atom code snippets:

```
'.source.js':
  'console.log':
    'prefix': 'log'
    'body': 'console.log(\'${1:}\');$2'

  '// -----------------------------------------------':
    'prefix': 'dem'
    'body': '// -----------------------------------------------\n// ${1:}'

'.source.html':
  'angular translate tag':
    'prefix': '--'
    'body': '{{  | translate }}'
```

## Tasks


### Task 0: Getting prepared

1. Get the git repository from your mentor.
2. __<font color="blue">Please Read read through the entire probation document before you begin</font>__.

### Task 1: Warm up

#### 1.1 Write a HTML file that confronts W3C Standard

* Estimated Task Length(hours): 5
* Goal: Learn W3C standard.
* Exercise: Write a HTML file that confronts W3C Standard, which includes the following tags

  1. body
  2. header
  3. html
  4. script

#### 1.2 Hello world

* Estimated Task Length(hours): 1
* Goal: Learn how to write java script, and output console log.
* Exercise: Followed by Task 1-1, write a javascript function that outputs "hello world" to console log.

#### 1.3 Equation Calculator

* Estimated Task Length(hours): 2
* Goal: Basic front end training.
* Exercise: Followed by Task 1-2, create a page that calculates ax + by = c:

![Task 1-3](./images/task_1_3_1.png)

* __Do not use button__ for calculation the result, use event listener to fetch input text change and update the result.
* __Do not use alert__ for showing the result, use the format as figure shows.
* __Do not use any third party css__ or javascript plugins during the task.
* Spacing between label and input field should be __at least 25px__ wide.

#### 1.4 Equation Calculator with error handling

* Estimated Task Length(hours): 8
* Goal: Basic front end training.
* Exercise: Followed by Task 1-3, modify the code to prevent input error:

![Task 1-4](./images/task_1_4_1.png)

* Note: only update result when all the field was correctly validated, if there is any invalid field, please show N/A as result.
* __Do not use any third party css__ or javascript plugins during the task.

#### 1.5 Equation Calculator -- Code Refactoring

* Estimated Task Length(hours): 2
* Goal: Basic front end training.
* Exercise: Followed by Task 1-4, modify the code to meet the following requirement:

  * Separate javascript code into individual file.
	* Separate CSS into individual file.
	* No style should appear in HTML tags, use class or id for mapping style in CSS,

	  ex:
    ![Task 1-5](./images/task_1_5_1.png)

#### 1.6 Equation Calculator -- Third party components

* Estimated Task Length(hours): 16
* Goal: Basic front end training.
* Exercise: Followed by Task 1-5, modify the code to meet the following requirement:

  * Use third party css component: Bootstrap.
	* Modify the UI to meet spec:

    ![Task 1-6](./images/task_1_6_1.png)
    ![Task 1-6](./images/task_1_6_2.png)

### Task 2: Modern Front End Development

#### 2.1 Environment & Tools

* Estimated Task Length(hours): 2
* Goal: Learn to utilize modern framework & tools for front end development.
* Exercise: Follow the [Youtube video](https://www.youtube.com/watch?v=z4JUm0Bq9AM) and finish up the lesson.
* Link to source code of tutorial is here: [Tutorial](https://coursetro.com/posts/code/154/Angular-6-Tutorial---Learn-Angular-6-in-this-Crash-Course)
* API url: https://jsonplaceholder.typicode.com/posts
* Note: please __solve__ the class.activated issue in video, and let your mentor know how you solved the issue.

#### 2.2 Equation Calculator with Angular 6 scaffolding

* Estimated Task Length(hours): 4
* Goal: Rewrite Task 1 using angular 6 + typescript
* Please remember to modify task 2.1 result and make it __responsive__, just like task 1 requires.

#### 2.3 Equation Calculator with Angular & Angular Material

* Estimated Task Length(hours): 16
* Goal: Angular 6 & Angular material.
* Exercise:
  * With the code from Task from 2-2, __<font color="red">remove</font>__ bootstrap dependency and __<font color="blue">use angular material</font>__ to rewrite the project.
	* Also rewrite the input fields and use angular style, i.e: use two way binding.
	* You should read through the basic of angular & angular material documents, and get familiar with it.


#### 2.4 Equation Calculator with data visualization

* Estimated Task Length(hours): 16
* Goal: Data visualization with D3.js.
* Exercise:
  * With the code from Task from 2-3, and modify to meet the spec, please draw the chart with d3.js.

    ![Task 2-4](./images/task_2_4_1.png)

  * When input value changes, please update “a*x^2 + b*x + c”,

	  ex: a=3, b=4, c=5, __<font color="blue">The result of 3*x^2 + 4*x + 5</font>__

  * When input value changes, please update chart.
	* You can reference the following link for the visualization of [quadratic equation](http://www.softschools.com/math/algebra_1/quadratic_equation_graph/)

    ![Task 2-4](./images/task_2_4_2.png)

  * Please remember to make your chart __responsive__.

#### 2.5 Equation Calculator + yMin/yMax xMin/xMax setting

* Estimated Task Length(hours): 16
* Goal: Bind input value with chart redraw.
* Exercise:
  * With the code from Task from 2-4, and modify to meet the spec, please sample video for detailed spec.
  * Sample video: [2018_11_30 Angular7 + d3js equation calculator](https://www.youtube.com/watch?v=tE0PzEwJosA), curtesy by Stan Marsh

#### 2.6 Directive exercise

* Estimated Task Length(hours): 8
* Goal: Make the charting mechanism as a directive, e.g. <app-d3-line-chart></app-d3-line-chart>
* Exercise:
  * With the code from Task from 2-5, extract the equation calculator chart component into a directive.
  * With the help of directive, we can input data model into it the directive like:

    <app-d3-line-chart [lineChartData]="lineChartData"></app-d3-line-chart>

  * Please input data to draw [Sin wave](https://en.wikipedia.org/wiki/Sine_wave)
  * Goal is to utilize the same drawing directive and draw equation or sine wave.
